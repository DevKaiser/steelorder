extends Area

export var ROTATION_SPEED = 1

var enabled

func _ready():
	enabled = false

func _physics_process(delta):
	if enabled:
		rotate_y(ROTATION_SPEED * delta)

func _on_RailgunTurretPickUp_body_entered(body):
	if enabled:
		body.change_railgun_turret()
		queue_free()

func enable():
	enabled = true
	show()

