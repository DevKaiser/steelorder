extends Spatial

onready var railgunTurretPickUp = $RailgunTurretPickUp

func _ready():
	GameState.world = self
	GameState.player = $Player/Tank
	set_enemies()

func _process(delta):
	if (Input.is_action_just_pressed("escape")):
		if $Player/Tank/GamePaused.visible:
			$Player/Tank/GamePaused.visible = false
			get_tree().paused = false
		else:
			$Player/Tank/GamePaused.visible = true
			get_tree().paused = true
			
func set_enemies():
	if GameState.difficulty == 1:
		$EnemyTanks/EnemyTank05.hide()
		$EnemyTanks/EnemyTank06.hide()
		$EnemyTanks/EnemyTank07.hide()
		$EnemyTanks/EnemyTank08.hide()
		$EnemyTanks/EnemyTank09.hide()
		$EnemyTanks/EnemyTank10.hide()
	elif GameState.difficulty == 2:
		$EnemyTanks/EnemyTank08.hide()
		$EnemyTanks/EnemyTank09.hide()
		$EnemyTanks/EnemyTank10.hide()
