extends Control

var gameState

func _ready():
	gameState = $"/root/GameState"

func _on_StartGameButton_pressed():
	gameState.gameWon = false
	gameState.enemiesKilled = 0
	get_tree().change_scene("res://Scenes/Briefing.tscn")

func _on_QuitButton_pressed():
	get_tree().quit()

