extends Spatial

var isAlive
var player

onready var NormalShell = preload("res://Scripts/NormalShell.gd")

onready var chassis = $Tank/Chassis/Chassis
onready var turretObj = $Tank/Turret
onready var turret = $Tank/Turret/Turret
onready var cannon = $Tank/Turret/Cannon/Cannon

export var destroyedMaterial = preload("res://Materials/BlackDestroyed.tres");

func _ready():
	isAlive = true

func _process(delta):
	if isAlive:
		turretRotate()

func turretRotate():
	var player = GameState.player
	turretObj.look_at(player.global_transform.origin, Vector3.UP)
	turretObj.rotate_object_local(Vector3(0,1,0), 3.14)
	
func _on_Area_body_entered(body):
	if body is NormalShell:
		body.queue_free()
		if isAlive:
			destroy()

func destroy():
	isAlive = false
	
	GameState.enemy_killed()
	
	chassis.set_surface_material(0, destroyedMaterial)
	turret.set_surface_material(0, destroyedMaterial)
	cannon.set_surface_material(0, destroyedMaterial)
	
	var smokeInstance = preload("res://Scenes/Smoke.tscn").instance()
	turret.add_child(smokeInstance)
