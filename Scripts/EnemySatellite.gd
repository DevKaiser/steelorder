extends KinematicBody

var gameState
var isAlive

func _ready():
	gameState = $"/root/GameState"
	isAlive = true

func _process(delta):
	var world_origin = get_parent().global_transform.origin
	look_at(world_origin, Vector3.UP)

func _on_Area_body_entered(body):
	body.queue_free()
	destroyed()

func make_explosion():
	$Chassis.hide()
	$SolarPanel.hide()
	$Area.hide()
	var explosion = preload("res://Scenes/Explosion.tscn").instance()
	add_child(explosion)
	explosion.global_transform = global_transform
	explosion.emitting = true

func destroyed():
	if isAlive:
		isAlive = false
		make_explosion()
		start_endgame()

func start_endgame():
	$EndgameCountdown.start()

func _on_EndgameCountdown_timeout():
	gameState.gameWon = true
	get_tree().change_scene("res://Scenes/EndGameMenu.tscn")
