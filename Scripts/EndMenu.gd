extends Control

var gameState

func _ready():
	gameState = $"/root/GameState"
	if gameState.gameWon:
		$VBoxContainer/Label.text = "You Win!"
	else:
		$VBoxContainer/Label.text = "Game Over"

func _on_MainMenuButton_pressed():
	get_tree().change_scene("res://Scenes/StartMenu.tscn")
	
func _on_QuitButton_pressed():
	get_tree().quit()
