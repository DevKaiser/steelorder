extends Control

func _on_EasyButton_pressed():
	GameState.difficulty = 1
	GameState.enemiesToKill = 4
	get_tree().change_scene("res://Scenes/World.tscn")

func _on_NormalButton_pressed():
	GameState.difficulty = 2
	GameState.enemiesToKill = 7
	get_tree().change_scene("res://Scenes/World.tscn")

func _on_HardButton_pressed():
	GameState.difficulty = 3
	GameState.enemiesToKill = 10
	get_tree().change_scene("res://Scenes/World.tscn")
