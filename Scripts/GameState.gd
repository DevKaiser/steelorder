extends Node

var gameWon
var world
var player
var difficulty
var enemiesToKill
var enemiesKilled

func _ready():
	gameWon = false
	difficulty = 1
	enemiesKilled = 0

func enemy_killed():
	enemiesKilled += 1
	check_pickUp_visible()

func get_remaining_enemies():
	return enemiesToKill - enemiesKilled
	
func check_pickUp_visible():
	if get_remaining_enemies() == 0:
		player.showPopUpText("Return to base and pick up the ion cannon")
		world.railgunTurretPickUp.enable()
