extends KinematicBody

var GRAVITY = 9.81
export var TANK_ACCELERATION = 5
export var TANK_ROTATION_SPEED = 0.5
export var TANK_MAX_SPEED = 30
export var TURRET_ROTATION_SPEED = 1
export var CANNON_ELEVATION_SPEED = 1
export var CANNON_MINIMUM_ELEVATION = -5
export var CANNON_MAXIMUM_ELEVATION = 15
export var RAILGUN_MINIMUM_ELEVATION = 0
export var RAILGUN_MAXIMUM_ELEVATION = 70
export var FRICTION_CONSTANT = 0.10
export var SHELL_INITIAL_VELOCITY = 3000

var GREEN_RELOAD_COLOR = Color("39bc0f")
var ORANGE_RELOAD_COLOR = Color("d38b25")
var RED_RELOAD_COLOR = Color("e23321")

var velocity_dir = Vector3.ZERO		# Plane x-z
var velocity_y = 0
var velocity_module = 0

var activeCamera
var activeTurret
var activeCannon
var activeCannonFirePoint
var activeMinCannonElevation
var activeMaxCannonElevation
var reloading = false

onready var turretCamera = $Turret/TurretCamera
onready var turret = $Turret
onready var cannon = $Turret/Cannon
onready var railgunCamera = $RailgunTurret/RailgunTurret/Railgun/RailgunCamera
onready var railgunTurret = $RailgunTurret
onready var railgunCannon = $RailgunTurret/RailgunTurret/Railgun
onready var labelVelocityValue = $VelocityDisplay/ColorRect/VBoxContainer/HBoxVelocity/LabelVelocityValue
onready var labelRemainingTimeValue = $VelocityDisplay/ColorRect2/HBoxContainer2/RemainingTimeValue
onready var cannonFirePoint = $Turret/Cannon/CannonFirePoint
onready var railgunFirePoint = $RailgunTurret/RailgunTurret/Railgun/CannonFirePoint2
onready var cannonAngleValue = $VelocityDisplay/ColorRect/VBoxContainer/HBoxAngle/LabelAngleValue
onready var reloadTimer = $ReloadTimer
onready var colorReload = $VelocityDisplay/ColorRect/VBoxContainer/HBoxReload/ColorReload
onready var tanksRemainingValue = $VelocityDisplay/ColorRect/VBoxContainer/HBoxTanksRemaining/TanksRemainingValue
onready var popUpRect = $VelocityDisplay/PopUpRect
onready var popUpText = $VelocityDisplay/PopUpRect/PopUpText
onready var popUpTimer = $VelocityDisplay/PopUpTimer

func _ready():
	activeCamera = turretCamera
	turretCamera.current = true
	railgunCamera.current = false
	
	activeTurret = turret
	activeCannon = cannon
	activeCannonFirePoint = cannonFirePoint
	
	activeMinCannonElevation = CANNON_MINIMUM_ELEVATION
	activeMaxCannonElevation = CANNON_MAXIMUM_ELEVATION

func _physics_process(delta):
	gravity_force(delta)
	tank_control(delta)
	friction_force(delta)
	
	turret_control(delta)
	cannon_control(delta)
	
	velocity_dir = velocity_dir.normalized()
	var velocity = Vector3(velocity_module * velocity_dir.x, velocity_y, velocity_module * velocity_dir.z)
	move_and_slide(velocity, Vector3.UP)
	
	updateGUI()

func _process(delta):
	var fireAction = get_fire_input()
	if activeCannon == cannon:
		if (fireAction && !reloading):
			fireShell(fireAction)
			reloading = true
			reloadTimer.start()
	else:
		fireShell(fireAction)

func gravity_force(delta):
	velocity_y -= GRAVITY * delta
	velocity_y = clamp(velocity_y, -10, 10)		# Ideally set to zero when grounded

func tank_control(delta):
	var input_direction = get_tank_input()
	rotation_force(input_direction, delta)
	thrust_force(input_direction, delta)

func rotation_force(input_direction, delta):
	rotate_y(TANK_ROTATION_SPEED * -input_direction.x * delta)
	velocity_dir = transform.basis.z

func thrust_force(input_direction, delta):
	var tankDirectionForward = transform.basis.z
	velocity_module += TANK_ACCELERATION * input_direction.z * delta
	velocity_module = clamp(velocity_module, -TANK_MAX_SPEED, TANK_MAX_SPEED)

func friction_force(delta):
	velocity_module += -FRICTION_CONSTANT * velocity_module * delta

func turret_control(delta):
	var input_direction = get_turret_input()
	activeTurret.rotate_y(TURRET_ROTATION_SPEED * -input_direction * delta)

func cannon_control(delta):
	var input_direction = get_cannon_input()
	activeCannon.rotate_x(CANNON_ELEVATION_SPEED * -input_direction * delta)
	activeCannon.rotation_degrees.x = clamp(activeCannon.rotation_degrees.x, -activeMaxCannonElevation, -activeMinCannonElevation)

func fireShell(fired):
	var ammo
	if activeCannon == cannon:
		ammo = preload("res://Scenes/Shells/NormalShell.tscn").instance()
		get_tree().get_root().get_node("World").add_child(ammo)
		ammo.global_transform = activeCannonFirePoint.global_transform
		var shell_velocity_direction = activeCannonFirePoint.global_transform.basis.z
		ammo.look_at(shell_velocity_direction, Vector3.UP)
		ammo.apply_central_impulse(SHELL_INITIAL_VELOCITY * shell_velocity_direction)
	else:
		ammo = preload("res://Scenes/Laser.tscn").instance()
		activeCannonFirePoint.add_child(ammo)
		#ammo.global_transform = activeCannonFirePoint.global_transform
		#var ammo_velocity_direction = railgunCannon.global_transform.basis.z
		ammo.rotation_degrees.x = 90 
		
		var raycast = ammo.get_node("RayCast")
		if fired:
			ammo.emitting = true
			raycast.enabled = true
			raycast.force_raycast_update()
			var collision = raycast.get_collider()
			if collision != null && collision.get_parent() != null && collision.get_parent().name == "EnemySatellite":
				collision.get_parent().destroyed()
		else:
			ammo.emitting = false
			raycast.enabled = false
			

func get_tank_input():
	var dir_x = 0
	var dir_z = 0
	if (Input.is_action_pressed("ui_left")):
		dir_x -= 1
	if (Input.is_action_pressed("ui_right")):
		dir_x += 1
	if (Input.is_action_pressed("ui_up")):
		dir_z += 1
	if (Input.is_action_pressed("ui_down")):
		dir_z -= 1
	return Vector3(dir_x, 0, dir_z).normalized()
	
func get_turret_input():
	var direction = 0
	if (Input.is_action_pressed("turret_left")):
		direction -= 1
	if (Input.is_action_pressed("turret_right")):
		direction += 1
	return direction
	
func get_cannon_input():
	var direction = 0
	if (Input.is_action_pressed("cannon_down")):
		direction -= 1
	if (Input.is_action_pressed("cannon_up")):
		direction += 1
	return direction

func updateGUI():
	labelVelocityValue.text = str(int(velocity_module)) + " Km/h"
	labelRemainingTimeValue.text = str(int($EndgameTimer.time_left)) + "s"
	cannonAngleValue.text = str(int(-activeCannon.rotation_degrees.x)) + "º"

	if (reloadTimer.time_left <= 0):
		colorReload.color = GREEN_RELOAD_COLOR
	elif (reloadTimer.time_left <= 1.0):
		colorReload.color = ORANGE_RELOAD_COLOR
	else:
		colorReload.color = RED_RELOAD_COLOR
	
	tanksRemainingValue.text = str(GameState.get_remaining_enemies())

func get_fire_input():
	var fired = false
	if (Input.is_action_pressed("cannon_fire")):
		fired = true
	return fired

func _on_ResumeButton_pressed():
	$GamePaused.visible = false
	get_tree().paused = false

func _on_QuitButton_pressed():
	get_tree().quit()

func _on_EndgameTimer_timeout():
	get_tree().change_scene("res://Scenes/EndGameMenu.tscn")

func change_railgun_turret():
	$Turret.hide()
	$RailgunTurret.show()
	
	activeCamera = railgunCamera
	turretCamera.current = false
	railgunCamera.current = true
	
	activeTurret = railgunTurret
	activeCannon = railgunCannon
	activeCannonFirePoint = railgunFirePoint
	
	activeMinCannonElevation = RAILGUN_MINIMUM_ELEVATION
	activeMaxCannonElevation = RAILGUN_MAXIMUM_ELEVATION

func _on_ReloadTimer_timeout():
	reloading = false

func showPopUpText(text):
	popUpText.text = text
	popUpRect.show()
	popUpTimer.start()

func _on_PopUpTimer_timeout():
	popUpRect.hide()
